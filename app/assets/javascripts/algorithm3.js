let SCALE_X = 500;
let SCALE_Y = 0;
let RADIUS = 6;

var img;
var slider;
var radio;
var checkbox;

class Ball {
  constructor (c, pos_x, pos_y, radius) {
    this.c = c;
    this.starting_pos = createVector (pos_x, pos_y);
    this.pos = createVector (pos_x, pos_y);
    this.vel = createVector (random (-1, 1), random (-1, 1));
    this.radius = radius;
    this.starting_radius = radius;
    this.distance = 2*radius;
  }

  missDirect() {
    this.vel.x = random(-1,1);
    this.vel.y = random(-1,1);
    
    if (random(1) < 0.2)
        this.radius += 2;
    else if (this.radius > 5)
      this.radius -= 1;
  }
  
  show() {
    var val = slider.value();
    var rad = radio.value();
    fill ((red(this.c)+val)%256,(green(this.c)+val)%256,(blue(this.c)+val)%256);
    if (!checkbox.checked())
      noStroke();
    else {
      stroke (0);
    }
    if (rad == "Ellipse")
      ellipse (this.pos.x, this.pos.y, this.radius, this.radius);
    if (rad == "Square")
      rect (this.pos.x-this.radius/2.0, this.pos.y-this.radius/2.0, this.radius, this.radius);
    if (rad == "Triangle")
      triangle (this.pos.x, this.pos.y+this.radius, this.pos.x+this.radius*sin(PI/3.0), this.pos.y-this.radius*cos(PI/3.0), this.pos.x-this.radius*sin(PI/3.0), this.pos.y-this.radius*cos(PI/3.0));
    if (rad == "Line") {
      stroke (red(this.c),green(this.c),blue(this.c));
      line (this.pos.x, this.pos.y+this.radius, this.pos.x+this.radius*sin(PI/3.0), this.pos.y-this.radius*cos(PI/3.0));
    }
  }

  update() {
    if (this.pos.x+this.vel.x > this.starting_pos.x+this.distance || this.pos.x+this.vel.x < this.starting_pos.x - this.distance) {
      this.missDirect();
    }
    if (this.pos.y+this.vel.y > this.starting_pos.y+this.distance || this.pos.y+this.vel.y < this.starting_pos.y - this.distance) {
      this.missDirect();
    }
    if (this.pos.x < 0 || this.pos.x > width) {
      this.vel.x *= -1;
    }
    if (this.pos.y < 0 || this.pos.y > height) {
      this.vel.y *= -1;
    }
    this.pos.add(this.vel);    
  }
}

function preload () {
  let escalaX = 500;
  var url = document.getElementsByTagName("img")[0].src;

  img = loadImage(url, function(img){img.resize(escalaX,0);});
}

var balls = [];
function setup() {
  createCanvas (img.width, img.height);
  colorMode(RGB);
  background (255);
  img.loadPixels();
  slider = createSlider (0, 255, 0);
  slider.style ('width', '200px');
  radio = createRadio();
  radio.option ('Triangle');
  radio.option ('Ellipse');
  radio.option ('Square');
  radio.option ('Line');
  radio.value ('Triangle');
  checkbox = createCheckbox ('Contorno', false);

  for (var y = RADIUS/2.0; y < img.height; y += RADIUS) {
    for (var x = RADIUS/2.0; x < img.width; x += RADIUS) {
      var b = new Ball(img.get(floor(x),floor(y)), x, y, RADIUS);
      balls.push (b);
    }
  }

  img.updatePixels();
}

function draw(){
  balls.forEach(function(b){
    b.update();
  b.show();
  });
  console.log(radio.value());
}

function saveGif() {
  frameRate(20)
  createLoop({duration:3,gif:{render:false,startLoop:1,endLoop:2,download:true}})
}