var img;
var d;
var k;
var l;
var p;
var velocidade;

function preload(){
	velocidade = 50;
	var escalaX = 500;
  var url = document.getElementsByTagName("img")[0].src;

	img = loadImage(url, function(img){img.resize(escalaX,0);});
}

function setup() {
	var cnv = createCanvas(img.width, img.height);
	
	background(0);
	k = img.width/2;
	l = img.height/2;
	d = 15;
	p = 0;
}

function mousePressed(){
	k = mouseX;
	l = mouseY;
}

function draw() {
	var limiteXdir, limiteYcima;
	var limiteXesq, limiteYbaixo;
	for (var i = 0; i < velocidade; i++){
	k+d > img.width ? limiteXdir = img.width : limiteXdir = k+d;
	k-d < 0 ? limiteXesq = 0 : limiteXesq = k-d;
	l+d > img.height ? limiteYbaixo = img.height : limiteYbaixo = l+d;
	l-d < 0 ? limiteYcima = 0 : limiteYcima = l-d
  var x = random(limiteXesq ,limiteXdir);
  var y = random(limiteYcima, limiteYbaixo);
	var r;
  random(1) < p ? r = random(5) : r = random(1, 20);
	random(1) < 0.001 ? p += 0.1 : {};
	random(1) < 0.0001 ? p = 0.2 : {};
	k = x;
	l = y;
  noStroke();
  fill(img.get(x, y));
  ellipse(x, y, r, r);
	}
}