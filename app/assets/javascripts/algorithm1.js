var img;
var pao;
var size;
var aux;
var escalaX;

function preload(){
  escalaX = 500;
  var url = document.getElementsByTagName("img")[0].src;

  img = loadImage(url, function(img){img.resize(escalaX,0);});
}

function setup(){
  createCanvas (img.width, img.height);
  background (0);
    var url = document.getElementsByTagName("img")[0].src;
    pao = loadImage(url, function(img){img.resize(escalaX,0);});
    aux = loadImage(url, function(img){img.resize(escalaX,0);});
  frameRate(20);
  size = random(100,200);
}

function draw(){
  translate (width/2, height/2);
  rotate (random(TWO_PI));
  pao.copy(aux, 0,0,width,height,0,0,width,height);
  dynamicSize = random(10,30)
  pao.resize(size, dynamicSize); 
  image(pao, 0, 0); 
  if (random(1) < 0.85) {
    size = floor(random(100, 200));
  }
  else {
    size = floor(random(300, 400));
  }
}