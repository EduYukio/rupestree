class FileUploadsController < ApplicationController
  def new
    @gallery = Gallery.find(params[:gallery_id])
  end

  def create
    @gallery = Gallery.find(params[:gallery_id])
    @gallery.files.attach(params[:gallery][:files])
    redirect_to gallery_path(@gallery)
  end

  def destroy
    @gallery = Gallery.find(params[:gallery_id])
    @gallery.files.find(params[:id]).purge
    redirect_to gallery_path(@gallery)
  end

end
