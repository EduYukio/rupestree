class Gallery < ApplicationRecord
	validates :name, presence: true, allow_blank: false, uniqueness: { case_sensitive: false }

	has_many_attached :files
end