FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN apt-get update && apt-get install -y iceweasel wget

RUN mkdir /rupestree

# Set the working directory to /app
WORKDIR /rupestree

# Copy the current directory contents into the container at /app
COPY Gemfile /rupestree
COPY Gemfile.lock /rupestree/Gemfile.lock

RUN bundle install
RUN rails active_storage:install
RUN rails g haml:application_layout convert
COPY . /rupestree

# Make port 80 available to the world outside this container
EXPOSE 8001

# Define environment variable
ENV NAME ZE

# Run app.rb when the container launches
CMD ["rails", "server", "-b", "0.0.0.0"]

