class AddFieldsToGalleries < ActiveRecord::Migration[5.2]
  def change
    add_column :galleries, :owner, :string
  end
end
