module OmniauthMacros
  def mock_auth_hash
    # The mock_auth configuration allows you to set per-provider (or default)
    # authentication hashes to return during integration testing.
    OmniAuth.config.mock_auth[:google_oauth2] = nil

    omniHash = {
      'provider'            => 'TestProvider',
      'uid'                 => 'TestUID',
      'info' => {
        'first_name'       => 'TestName',
        'last_name'        => 'TestLastName',
        'email'            => 'TestEmail',
      }
    }
    OmniAuth.config.add_mock(:google_oauth2, omniHash)
    Rails.application.env_config["omniauth.auth"] = OmniAuth.config.mock_auth[:google_oauth2]
  end

  def mock_failure_auth_hash
    OmniAuth.config.mock_auth[:google_oauth2] = :invalid_credentials
  end
end