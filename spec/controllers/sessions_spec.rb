require 'rails_helper'

describe "login with valid credentials" do
  before(:each) do
    visit_home_and_mock_user("success")
  end

  it {expect(response).to render_template("index")}
  it {check_contents(index_page_without_gallery_contents)}

  it {expect(page).not_to have_content("RUPESTREE")}
end

describe "login with invalid credentials" do
  before(:each) do
    visit_home_and_mock_user("failure")
  end

  it {expect(response).to render_template("show")}
  it {check_contents(home_contents)}

  it {expect(page).not_to have_content("profile")}
  it {expect(page).not_to have_content("TestName")}
end

describe "logout" do
  before(:each) do
    visit_home_and_mock_user("success")
    click_link "Logout"
  end

  it {check_contents(home_contents)}

  it {expect(page).not_to have_content("profile")}
  it {expect(page).not_to have_content("TestName")}

  context "try to login again" do
    before(:each) do
      click_link "Sign in with Google"
    end

    it {check_contents(index_page_without_gallery_contents)}

    it {expect(page).not_to have_content("RUPESTREE")}
  end
end
