require 'rails_helper'

describe "accessing index" do
  before(:each) do
    mock_user_and_visit_galleries
  end

  it {expect(response).to render_template("index")}
  it {check_contents(index_page_without_gallery_contents)}
end



describe "creating new gallery" do
  before(:each) do
    mock_user_and_visit_galleries
    click_link "New Gallery"
  end

  it {expect(response).to render_template("new")}
  it {check_contents(new_page_contents)}

  context "with a valid name" do
    before(:each) do
      fill_and_confirm_with_name("GalleryTest1")
    end

    it {expect(response).to render_template("show")}
    it {check_contents(show_page_contents)}
  end

  context "with an existing name" do
    before(:each) do
      fill_and_confirm_with_name("GalleryTest1")
      click_link("Back")
      create_gallery_with_name("GalleryTest1")
    end
    
    it {expect(page).to have_content("Name has already been taken")}
    it {expect(page).to have_content("error prohibited this gallery from being saved")}
    it {expect(page).not_to have_content("Listing galleries")}
    it {expect(page).not_to have_content("Gallery was successfully created.")}
  end

  context "with a blank name" do
    before(:each) do
      fill_and_confirm_with_name("")
    end
    
    it {expect(page).to have_content("Name can't be blank")}
    it {expect(page).to have_content("error prohibited this gallery from being saved")}
    it {expect(page).not_to have_content("Listing galleries")}
    it {expect(page).not_to have_content("Gallery was successfully created.")}
  end
end



describe "showing a gallery" do
  before(:each) do
    mock_user_and_visit_galleries
    create_gallery_with_name("GalleryTest1")
  end

  it {expect(response).to render_template("show")}
  it {check_contents(show_page_contents)}
end



describe "editing a gallery" do
  before(:each) do
    mock_user_and_visit_galleries
    create_gallery_with_name("GalleryTest1")
    click_link "Edit"
  end

  it {expect(response).to render_template("edit")}
  it {check_contents(edit_page_contents)}

  context "with a different name" do
    before(:each) do
      fill_and_confirm_with_name("GalleryTest2")
    end

    it {expect(response).to render_template("show")}
    it {expect(page).to have_content("GalleryTest2")}
    it {expect(page).not_to have_content("GalleryTest1")}
  end

  context "with the same name" do
    before(:each) do
      fill_and_confirm_with_name("GalleryTest1")
    end
    
    it {expect(response).to render_template("show")}
    it {check_contents(show_page_contents)}
  end

  context "with a blank name" do
    before(:each) do
      fill_and_confirm_with_name("")
    end
    
    it {expect(page).to have_content("Name can't be blank")}
    it {expect(page).to have_content("error prohibited this gallery from being saved")}
    it {expect(page).not_to have_content("Listing galleries")}
  end

  context "when user clicks on Back" do
    before(:each) do
      click_link("Back")
    end

    it {expect(response).to render_template("index")}    
    it {check_contents(index_page_with_gallery_contents)}
  end
end



describe "destroying a gallery", :js => true, :driver => :selenium_headless do
  before(:each) do
    mock_user_and_visit_galleries
    create_gallery_with_name("GalleryTest1")
    click_link "Back"
  end

  context "when user dismisses confirmation popup" do
    before(:each) do
      dismiss_confirm{click_link "Destroy"}
    end

    it {expect(page).to have_content("GalleryTest1")}
  end

  context "when user accepts confirmation popup" do
    before(:each) do
      accept_confirm{click_link "Destroy"}
    end

    it {expect(page).not_to have_content("GalleryTest1")}
  end
end



describe "uploading images" do
  before(:each) do
    mock_user_and_visit_galleries
    create_gallery_with_name("GalleryTest1")
    click_link "Upload Files"
  end
  
  it {expect(page).to have_content("Upload for")}
  it {expect(page).to have_content("GalleryTest1")}

  bunnies = %w(small_bunny.jpg medium_bunny.jpg large_bunny.jpg)
  bunnies.each do |bunny|
    context ("with " + bunny + "resolution") do
      before(:each) do
        attach_file_and_confirm(files_to_upload_path + bunny)
      end

      it {check_contents(show_page_contents)}
      it {expect(page).to have_content("Delete image")}

      it {expect(page).to have_xpath("//img[contains(@src," + bunny + ")]")}
    end
  end

  context "with many resolutions" do
    before(:each) do
      file_paths = [small_bunny_path, medium_bunny_path, large_bunny_path]
      attach_file_and_confirm(file_paths)
    end

    it {check_contents(show_page_contents)}
    it {expect(page).to have_content("Delete image")}

    it {expect(page).to have_xpath("//img[contains(@src,'small_bunny.jpg')]")}
    it {expect(page).to have_xpath("//img[contains(@src,'medium_bunny.jpg')]")}
    it {expect(page).to have_xpath("//img[contains(@src,'large_bunny.jpg')]")}
  end
end



describe "deleting an image", :js => true, :driver => :selenium_headless do
  before(:each) do
    mock_user_and_visit_galleries
    create_gallery_with_name("GalleryTest1")
    upload_file_with_name(Rails.root + small_bunny_path)
  end

  context "when user dismisses confirmation popup" do
    before(:each) do
      dismiss_confirm{click_link "Delete image"}
    end

    it {expect(page).to have_xpath("//img[contains(@src,'small_bunny.jpg')]")}
  end

  context "when user accepts confirmation popup" do
    before(:each) do
      accept_confirm{click_link "Delete image"}
    end

    it {expect(page).not_to have_xpath("//img[contains(@src,'small_bunny.jpg')]")}
  end
end



describe "accessing manipulate page" do
  before(:each) do
    mock_user_and_visit_galleries
    create_gallery_with_name("GalleryTest1")
    upload_file_with_name(small_bunny_path)
  end
  
  it {check_contents(show_page_contents)}
  it {expect(page).to have_xpath("//img[contains(@src,'small_bunny.jpg')]")}

  context "when user clicks on an image" do
    before(:each) do
      find(:xpath, "//a[contains(@href,'manipulate')]").click
    end

    it {check_contents(manipulate_page_contents)}
    it {expect(page).to have_xpath("//img[contains(@src,'small_bunny.jpg')]")}
  end
end



describe "applying algorithms to an image", :js => true, :driver => :selenium_headless do
  before(:each) do
    mock_user_and_visit_galleries
    create_gallery_with_name("GalleryTest1")
    upload_file_with_name(Rails.root + small_bunny_path)
    find(:xpath, "//a[contains(@href,'manipulate')]/..").click
  end
  algorithms = %w(1 2 3)
  algorithms.each do |num|
    context ("when user applies algorithm " + num) do
      before do
        click_link ("Apply Algorithm " + num)
        page.driver.browser.navigate.refresh
      end

      it "works" do
        expect(page).to have_xpath("//canvas[contains(@class,'p5Canvas')]")

        if num == "1"
          expect(response).to render_template("apply_algorithm")
        else
          expect(response).to render_template("apply_algorithm" + num)
        end
      end
    end
  end
end
