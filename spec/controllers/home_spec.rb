require 'rails_helper'

describe "accessing home" do
	before(:each) do
  	visit '/home'
	end

  it {expect(response).to render_template("show")}

  it "has all the home content" do
    home_contents.each do |content|
      expect(page).to have_content content
    end
  end
end
