module SpecTestHelper

  # User
  def visit_home_and_mock_user(status)
    visit '/home'
    
    if status == "success"
      mock_auth_hash
    elsif status == "failure"
      mock_failure_auth_hash
    end
    
    click_link "Sign in with Google"
  end

  def mock_user_and_visit_galleries()
    visit_home_and_mock_user("success")
    visit '/galleries'
  end


  # Fill and confirm
  def fill_and_confirm_with_name(name)
    fill_in("Name", :with => name)
    find('input[name="commit"]').click
  end

  def attach_file_and_confirm(name)
    attach_file name
    find('input[name="commit"]').click
  end


  # Click, fill and confirm
  def create_gallery_with_name(name)
    click_link "New Gallery"
    fill_and_confirm_with_name(name)
  end

  def upload_file_with_name(name)
    click_link "Upload Files"
    attach_file_and_confirm(name)
  end


  # Files path
  def files_to_upload_path()
    "spec/files_to_upload/"
  end

  def small_bunny_path()
    "spec/files_to_upload/small_bunny.jpg"
  end

  def medium_bunny_path()
    "spec/files_to_upload/medium_bunny.jpg"
  end

  def large_bunny_path()
    "spec/files_to_upload/large_bunny.jpg"
  end


  # Page contents
  def home_contents()
    ['RUPESTREE', 'Edit your images applying generative algorithms', 'Sign in with Google']
  end

  def index_page_without_gallery_contents()
    ['TestName', 'profile', 'Listing galleries', "New Gallery", "Logout"]
  end

  def new_page_contents()
    ["New gallery", "Back"]
  end

  def index_page_with_gallery_contents()
    index_page_without_gallery_contents + ["GalleryTest1", 'Destroy', 'Show', 'Edit']
  end

  def show_page_contents()
    ["GalleryTest1", 'Back', 'Upload Files', 'Edit', "Click on an image to manipulate it"]
  end

  def edit_page_contents()
    ['Back', 'Editing gallery', "Name"]
  end

  def manipulate_page_contents()
    ['Manipulation page', "Apply Algorithm"]
  end
  

  # Check contents
  def check_contents(contents)
    contents.each do |content|
      expect(page).to have_content content
    end
  end
end