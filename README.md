# Rupestree

### Installation

This application runs in docker, so first of all, if you do not have it installed yet, run:

for Ubuntu or any Debian based distros:  
```$ sudo apt-get install docker docker-compose```

for distros based in ArchLinux, (or arch itself):  
```$ sudo pacman -S docker docker-compose```

Download or clone our repository:  
`https://gitlab.com/EduYukio/rupestree.git`

Enter the directory:
`$ cd rupestree`

And run those docker-compose commands to setup the application:
```
$ sudo docker-compose build
$ sudo docker-compose run web rake db:create
$ sudo docker-compose run web rails db:migrate
$ sudo docker-compose up
```

To see our app running, access:

Locally: http://localhost:3000/

Online: http://rupestree.herokuapp.com



### Instructions

Now, login with a google account.

You will be redirected to your profile account, in which you will be able to create galleries.

Click in "New Gallery" and give it a name.

You will land on that gallery page, being able to upload images to it by clicking on the "Upload Files" link.

Now you can click on an image to enter into the manipulation page, in which you will be able to apply algorithms to manipulate the image in curious ways.

**PS: Please, use the Firefox browser to run our project. The algorithms are not being correctly applied on Chrome and Safari because of security issues involving requests to the Amazon Web Services.**

### Testing

We did all of the tests using the RSpec framework.

So, to test our app, run:
`
$ docker-compose run web rake spec
`