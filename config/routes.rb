Rails.application.routes.draw do
  root 'home#show'
  get 'home', to: 'home#show'

  get 'me', to: 'galleries#index', as: 'me'
  
  get '/galleries/apply_algorithm', to: 'galleries#apply_algorithm' 
  get '/galleries/apply_algorithm2', to: 'galleries#apply_algorithm2' 
  get '/galleries/apply_algorithm3', to: 'galleries#apply_algorithm3' 
  get '/galleries/manipulate', to: 'galleries#manipulate'

  resources :galleries do
    resources :file_uploads, only: [:new, :create, :destroy]
  end

  get 'users/whoami', to: 'user#whoami'
  get 'login', to: redirect('/auth/google_oauth2'), as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
end
